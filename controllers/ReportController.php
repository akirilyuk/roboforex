<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 01.05.18
 * Time: 16:21
 */

namespace app\controllers;


use app\models\ReportService;
use app\models\UploadForm;
use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;

class ReportController extends Controller
{
    public function actionUpload()
    {
        $uploadForm = new UploadForm();

        $profiPoints = [];
        if (Yii::$app->request->isPost) {
            $uploadForm->reportFile = UploadedFile::getInstance($uploadForm, 'reportFile');
            if ($uploadForm->upload()) {

                try {
                    $reportService = new ReportService();
                    $reportService->parseFile($uploadForm->getFilePath());
                    $profiPoints = $reportService->getProfitPoints();
                } catch (\Exception $e) {
                    Yii::error($e->getMessage());
                }

                if (!$profiPoints) {
                    $uploadForm->addError('reportFile', "Файл с неправильной структурой");
                }
            }
        }

        return $this->render('upload', [
            'uploadForm' => $uploadForm,
            'points'     => $profiPoints,
        ]);
    }
}