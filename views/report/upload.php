<?php

/** @var \app\models\UploadForm $uploadForm */

use yii\widgets\ActiveForm;

?>

<?php $this->title = "Отчет по сделкам"; ?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

<?= $form->field($uploadForm, 'reportFile')
    ->fileInput() ?>

    <button>Выполнить</button>

<?php ActiveForm::end() ?>

<?php if ($points): ?>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        var points = <?php echo \yii\helpers\Json::encode($points);?>;
        console.log(points);
        google.charts.load('current', {'packages': ['line']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var dataTable = new google.visualization.DataTable();
            dataTable.addColumn('string', 'Дата');
            dataTable.addColumn('number', 'Прибыль');
            dataTable.addRows(points);

            var options = {
                chart: {
                    title: 'График роста баланса',
                    subtitle: 'в долларах США'
                },
                width: 900,
                height: 500
            };

            var chart = new google.charts.Line(document.getElementById('chart'));

            chart.draw(dataTable, google.charts.Line.convertOptions(options));
        }
    </script>
    <div id="chart"></div>
<?php endif; ?>