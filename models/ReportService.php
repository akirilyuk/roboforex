<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 01.05.18
 * Time: 17:11
 */

namespace app\models;


class ReportService
{
    const TYPE_BUY = "buy";
    const TYPE_BALANCE = "balance";
    const TYPE_SELL = "sell";
    const COLUMN_OPEN_TIME_NUMBER = 1;

    private static $_availableOperations = [
        self::TYPE_BUY,
        self::TYPE_BALANCE,
        self::TYPE_SELL
    ];

    private $_profitPoints = [];

    public function getProfitPoints()
    {
        return $this->_profitPoints;
    }

    public function setProfitPoint($closeTime, $amount)
    {
        $this->_profitPoints[] = [$closeTime, $amount];
    }

    /**
     * @param string $filePath
     * @return bool
     */
    public function parseFile($filePath)
    {
        $dom = new \DOMDocument();
        $dom->loadHTMLFile($filePath);
        $table = $dom->getElementsByTagName("table")
            ->item(0);

        if (!$table) {
            return false;
        }

        $profit    = 0;
        $tableRows = $table->getElementsByTagName("tr");

        /** @var \DOMElement $tableRow */
        foreach ($tableRows as $tableRow) {
            $tableColumns = $tableRow->getElementsByTagName("td");

            /** @var \DOMElement $tableColumn */
            foreach ($tableColumns as $tableColumn) {
                $timeColumn = $profitColumn = null;

                if (in_array($tableColumn->nodeValue, self::$_availableOperations)) {
                    $timeColumn   = $tableColumns->item(self::COLUMN_OPEN_TIME_NUMBER);
                    $profitColumn = $tableColumns->item($tableColumns->length - 1);
                }

                if (!$timeColumn || !$profitColumn) {
                    continue;
                }

                $profit    += $this->normalizeProfitValue($profitColumn->nodeValue);
                $timeParts = explode(" ", $timeColumn->nodeValue);
                $this->setProfitPoint($timeParts[0], $profit);
            }
        }
    }

    private function normalizeProfitValue($profitValue) {
        return floatval(preg_replace("/[^-\d\.]/","",$profitValue));
    }

}