<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 01.05.18
 * Time: 16:22
 */

namespace app\models;


use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $reportFile;

    public function rules()
    {
        return [
            [['reportFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'html, htm'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'reportFile' => "Файл для формирования графика"
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->reportFile->saveAs($this->getFilePath());
            return true;
        } else {
            return false;
        }
    }

    protected function getUploadDirectory()
    {
        $uploadDirectory = \Yii::$app->basePath . '/uploads/'. date("Y-m-d") . "/";
        if (!file_exists($uploadDirectory)) {
            mkdir($uploadDirectory, 0777, true);
        }
        return $uploadDirectory;
    }

    public function getFilePath()
    {
        return $this->getUploadDirectory() . $this->reportFile->baseName . '.' . $this->reportFile->extension;
    }
}